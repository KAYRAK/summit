package com.ykb.summitapp2;

import com.google.firebase.database.Exclude;

import java.io.Serializable;

/**
 * Created by akayrak on 03/05/2017.
 */

public class ChatMessage implements Serializable {

    public String content;
    public String username;

    public ChatMessage() {
    }

    public ChatMessage(String content, String username) {
        this.content = content;
        this.username = username;
    }
}
