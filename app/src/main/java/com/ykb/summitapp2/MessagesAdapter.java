package com.ykb.summitapp2;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by akayrak on 03/05/2017.
 */

public class MessagesAdapter extends ArrayAdapter<ChatMessage> {

    public MessagesAdapter(@NonNull Context context, @NonNull ArrayList<ChatMessage> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        ChatMessage chatMessage = getItem(position);

        if (convertView == null)
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.chat_item, parent, false);

        ((TextView) convertView.findViewById(R.id.username)).setText(chatMessage.username);
        ((TextView) convertView.findViewById(R.id.content)).setText(chatMessage.content);

        return convertView;
    }
}
